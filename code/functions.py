class Word:
    def __init__(self, line):
        parts = line.split()
        self.word = parts[0]
        self.sounds = parts[1:]

    #  returns the index of the first occurance of a sublist of sounds.
    #  returns -1 if not found
    def index_sounds(self, subSounds):
        lenSounds = len(self.sounds)
        lenSubSounds = len(subSounds)
        for i in range(lenSounds - lenSubSounds + 1):
            if subSounds == self.sounds[i:i+lenSubSounds]:
                return i
        return -1


    def create_portmanteau(self, index, otherWord, otherindex):
        sounds = ' '.join(self.sounds[:index] + otherWord.sounds[otherindex:])
        return {'word1': self.word, 'word2': otherWord.word, 'sounds': sounds}


#  pass a word and get back the word object
#  returns the word 'undiscovered' if the passed word wasnt found
def get_word(word):
    infile = open('../cmudict/cmudict.dict', 'r')
    for line in infile:
        if line.split()[0] == word:
            infile.close()
            return Word(line)
    infile.close()
    return(get_word('undiscovered'))


#  make this a generator
def find_portmanteaus_after(word, sounds_start, sounds_end):
    infile = open('../cmudict/cmudict.dict', 'r')
    sounds = word.sounds[sounds_start:sounds_end]
    print(sounds)
    rtnlst = []
    for line in infile:
        new_word = Word(line)
        index = new_word.index_sounds(sounds)
        if index > -1:
            rtnlst.append(word.create_portmanteau(sounds_start, new_word, index))
    return rtnlst
#  make this a generator


def find_portmanteaus_before(word, sounds_start, sounds_end):
    infile = open('../cmudict/cmudict.dict', 'r')
    sounds = word.sounds[sounds_start:sounds_end]
    print(sounds)
    rtnlst = []
    for line in infile:
        new_word = Word(line)
        index = new_word.index_sounds(sounds)
        if index > -1:
            rtnlst.append(new_word.create_portmanteau(index, word, sounds_start))
    return rtnlst


def filter_out_sounds(port_list, sounds):
    #  return [port['sounds'] != sounds for port in port_list]
    rtnlst = []
    for port in port_list:
        #  you gotta either split the sounds from the dict or join the param sounds
        if port['sounds'] != ' '.join(sounds):
            rtnlst.append(port)
    return rtnlst


def test():
    word = get_word(input('Enter a word: '))
    print(word.word, word.sounds, len(word.sounds))
    #  there's some way to do this like: index1, index2 = input('Enter indicies (comma separated): ')
    index1 = int(input('Enter index1: '))
    index2 = int(input('Enter index2: '))
    port_list = find_portmanteaus_before(word, index1, index2) + find_portmanteaus_after(word, index1, index2)
    #  this removes portmanteaus that have the same pronunciation as the original word
    port_list = filter_out_sounds(port_list, word.sounds)
    print('\n'.join([str(x) for x in port_list]))


test()

Portmanteau (Working Title)

A portmanteau generator based on phonetics. Uses the Carnegie Mellon Pronouncing Dictionary as a source for the pronunciation of words.
That project (https://github.com/cmusphinx/cmudict) needs to be cloned in the root directory of this project.

User should be allowed to generate portmanteaus based on a chosen word.

They should also be able to specify if they want letter loss.
e.g. brogrammer vs. programmermaid